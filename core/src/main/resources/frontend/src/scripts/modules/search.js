var axios = require('axios')
var _ = require('lodash')

class Search {
    constructor (keyword) {
        this.keyword = keyword
    }
    getResults () {
        console.log(this.keyword)
    }
}

function init() {
    const keyword = 'army'
    const setSearch = new Search(keyword)
    setSearch.getResults()
}


export { init }
// global var
import jQuery from "jquery"

window.$ = window.jQuery = jQuery
var _ = require('lodash')

// styles
import 'styles/main.scss'

// js
import 'bootstrap'

// imports
import utils from 'modules/utils.js' 
import { init as search } from 'modules/search.js'; 


// libs


// run module
utils.init()
search() 

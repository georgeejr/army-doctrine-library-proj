const merge = require('webpack-merge');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const baseConfig = require('./webpack.config.base.js');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin')



const extractCSS = new ExtractTextPlugin({
	filename: 'css/main.css',
});

// TODO: remove all console.log upon build
module.exports = merge(baseConfig, {
	module: {
		rules: [
			{
				test: /\.scss$/,
				use: extractCSS.extract({
					use: [
						'css-loader',
						'postcss-loader',
						{
							loader: 'sass-loader',
							options: {
								indentWidth: 4,
								outputStyle: 'expanded',
							}
						},
					],
				})
			}
		],
	},
	plugins: [
		extractCSS,
		new StyleLintPlugin({
			configFile: './stylelint.config.js',
			syntax: 'scss',
		}),
		new CopyWebpackPlugin([
			{ from: './src/js/', to: 'js/', toType: 'dir' },
			{ from: './src/css/', to: 'css/', toType: 'dir' },
			{ from: './src/resources/img/', to: 'img/', toType: 'dir' },
		], {}),
	],
});

  
